---
tags:
- special
---
Tagging system example repo
====

Content compatible with engine version 0.0.6 (down to 0.0.2), see [tagging system v0.0.6](https://gitlab.com/iguillaumed/playground/tagging/-/tree/v0.0.6).

<del>See [the currently used engine's docs](.tagging-engine/README.md).</del>
[Submodules are not supported in Gollum](https://github.com/gollum/gollum/wiki/Known-limitations#miscellaneous)
and GitLab does not show them either.